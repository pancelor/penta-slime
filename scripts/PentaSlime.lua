local dev,dev_convert_all
-- dev=true
dev_convert_all=dev

local action = require "necro.game.system.Action"
local attack = require "necro.game.character.Attack"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local components = require "necro.game.data.Components"
local confusion = require "necro.game.character.Confusion"
local currentLevel = require "necro.game.level.CurrentLevel"
local customEntities = require "necro.game.data.CustomEntities"
local damage = require "necro.game.system.Damage"
local ecs = require "system.game.Entities"
local event = require "necro.event.Event"
local invincibility = require "necro.game.character.Invincibility"
local move = require "necro.game.system.Move"
local object = require "necro.game.object.Object"
local rng = require "necro.game.system.RNG"
local spell = require "necro.game.spell.Spell"
local spellItem = require "necro.game.item.SpellItem"
local voice = require "necro.audio.Voice"

local enemyPool = require "enemypool.EnemyPool"

if dev then
  event.levelLoad.add("spawn", {order="entities"},function (ev)
    object.spawn("pentaslime_slime",-1,-1)
  end)
end

event.levelLoad.add("registerPentaSlime", {order="entities"}, function(ev)
  enemyPool.registerConversion{from="Slime3",to="pentaslime_slime",probability=0.3}
  if dev_convert_all then
    enemyPool.registerConversion{from="Slime",to="pentaslime_slime"}
  end
end)

customEntities.extend {
  name="slime",
  template=customEntities.template.enemy("slime",3),
  components={
    friendlyName={name="Pentagon Slime"},
    innateAttack={damage=2},
    health={health=5, maxHealth=5},
    dropCurrencyOnDeath={amount=5},
    aiPattern={
      moves={
        action.Direction.DOWN_RIGHT,
        action.Direction.DOWN_LEFT,
        action.Direction.UP_LEFT,
        action.Direction.UP,
        action.Direction.RIGHT,
      },
    },
    dig={
      silentFail=true,
      strength=1,
    },
    sprite={
      texture="mods/pentaslime/images/penta-slime.png",
    },
  },
}
